export class Registration {
  id: string;
  registrationDate: string;
  ageRange: number;
  isResident: boolean;
  zipCode: string;
  socialSecurity: boolean;
  tanfeadc: boolean;
  snap: boolean;
  wic: boolean;
  sfsp: boolean;
  schoolBreakfast: boolean;
  schoolLunch: boolean;
  financialAid: boolean;
  otherBenefit: string;
  employed: boolean;
  houseHoldSize: number;
}
